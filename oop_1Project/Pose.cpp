#include "Pose.h"



Pose::Pose()
{
}


Pose::~Pose()
{
}

float Pose::getX()
{
	return x;
}

void Pose::setX(float _x)
{
	x = _x;
}

float Pose::getY()
{
	return y;
}

void Pose::setY(float _y)
{
	y = _y;
}

void Pose::setTh(float _th)
{
	th = _th;
}

float Pose::getTh()
{
	return th;
}

bool Pose::operator==(const Pose & other)
{
	if ((this->x == other.x) && (this->y == other.y) && (this->th == other.th))
		return 1;
	else
		return false;
}

Pose Pose::operator+(const Pose & other)
{
	Pose tmp;
	tmp.x = this->x + other.x;
	tmp.th = this->th + other.th;
	tmp.y = this->y + other.y;
	return tmp;//return do�ru mu bilmiyorum.
}

Pose Pose::operator-(const Pose & other)
{
	Pose tmp;
	tmp.x = this->x - other.x;
	tmp.th = this->th - other.th;
	tmp.y = this->y - other.y;
	return tmp;//return do�ru mu bilmiyorum.
}

Pose & Pose::operator+=(const Pose & other)
{
	this->x = this->x + other.x;
	this->th = this->th + other.th;
	this->y = this->y + other.y;
	return *this;//return yap�lacak
}

Pose & Pose::operator-=(const Pose & other)
{
	this->x = this->x - other.x;
	this->th = this->th - other.th;
	this->y = this->y - other.y;
	return *this;//return yap�lacak
}

bool Pose::operator<(const Pose &)
{
	//bu fonksiyonda hangisi buyuk olmal�.
	return false;
}

void Pose::getPose(float & _x, float & _y, float & _th)
{
	_x = this->x;
	_y = this->y;
	_th = this->th;
}

void Pose::setPose(float _x, float _y, float _th)
{
	this->x = _x;
	this->y = _y;
	this->th = _th;
}

float Pose::findDistanceTo(Pose pos)
{
	return pow((pow(pos.x, 2) + pow(pos.y, 2)), 0.5);
}

float Pose::findAngleTo(Pose pos)
{
	return pos.th - this->th;
}