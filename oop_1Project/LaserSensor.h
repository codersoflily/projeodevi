#pragma once
#include"PioneerRobotAPI.h"

//bur satir silinecek.

class LaserSensor
{
private:
	float ranges[181];
	PioneerRobotAPI*robotAPI;
public:
	float getRange(int index);
	void updateSensor(float ranges[]);
	float getMax(int &index);
	float getMin(int &index);
	float operator[](int i);
	float getAngle(int index);
	float getClosestRange(float startAngle, float endAngle, float& angle);
};

