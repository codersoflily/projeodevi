#include "RobotOperator.h"
#include "Encryption.h"
#include<iostream>
using namespace std;

int RobotOperator::encryptCode(int number)
{
	accessCode = enc.encrypt(number);
	return enc.encrypt(number);//enc is a object of Encryption
}
int RobotOperator::DecryptCode(int number)
{
	return enc.decrypt(number);
}
bool RobotOperator::checkAccessCode(int number)
{
	if (enc.encrypt(number) == accessCode) 
	{
		accessState = 1;
		return true;
	}
	else 
	{
		accessState = 0;
		return false;
	}
}

void RobotOperator::print()
{
	cout << "Name of Operator : " << name << endl;
	cout << "SurName of Operator : " << surname << endl;
	cout << "Access State of Operator : ";
	if (accessState == 1) 
	{
		cout << "Access Granted..."<<endl;
	}
	else 
	{
		cout << "Access Denied..." << endl;
	}
}