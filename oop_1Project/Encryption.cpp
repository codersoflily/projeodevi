#include "Encryption.h"
#include<iostream>
#include<cmath>
using namespace std;

int Encryption::encrypt(int number) 
{
	int digit[4], temp, encrypt = 0;
	int i, a;
	temp = number;
	//sayilar basamakalarina ayriliyor ve 7 ekleniyor
	for (i = 3; i >= 0; i--)
	{
		digit[i] = 7 + temp % 10;
		temp = temp / 10;
	}
	//basamaklarin yeri degisiyor
	for (i = 0; i < 2; i++) {
		temp = digit[i];
		digit[i] = digit[i + 2];
		digit[i + 2] = temp;
	}
	temp = 0;
	//dizi integer bir sayiya cevriliyor
	for (i = 0, a = 3; i <= 6; a--, i += 2)
	{
		temp = temp + digit[a] * pow(10, i);
	}
	return temp;
}

int Encryption::decrypt(int number)
{
	int temp = 0, re_digit[4];
	int i, a, basamak;
	//basamaklarına ayrilip 7 cıkariliyor
	temp = number;
	for (i = 0; i < 4; i++)
	{
		re_digit[i] = temp % 100;
		re_digit[i] = re_digit[i] - 7;
		temp = temp / 100;
	}

	temp = 0;
	//basamaklarin yeri düzenleniyor
	for (i = 0; i <= 2; i++) {
		temp = re_digit[i];
		re_digit[i] = re_digit[i + 1];
		re_digit[i + 1] = temp;
		i++;
	}

	temp = 0;
	//dizi integer sayiya cevriliyor
	for (i = 3, a = 0; i >= 0; i--)
	{
		basamak = re_digit[i];
		basamak = basamak * pow(10, a);
		temp = temp + basamak;
		a++;
	}
	return temp;//decrypted sayi
}
