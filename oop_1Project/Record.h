#pragma once
#ifndef RECORD_
#define RECORD_

class Record {
private:
	fileName string;
	fstream file;
public:
	bool openFile();
	bool closeFile();
	void setFileName(string name);
	string readLine();
	bool writeLine(string str);
	Record& operator<<();
	Record& operator>>();

	};

#endif // !RECORD_

