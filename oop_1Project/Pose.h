#pragma once
#include<math.h>
#include<iostream>
class Pose
{
public:
	float getX();
	void setX(float);
	float getY();
	void setY(float);
	void setTh(float);
	float getTh();
	bool operator==(const Pose& other);
	Pose operator+(const Pose& other);
	Pose operator-(const Pose& other);
	Pose& operator+=(const Pose& other);
	Pose& operator-=(const Pose& other);
	bool operator<(const Pose&);//!!!!!!!!!! go to defination
	void getPose(float& _x, float& _y, float& _th);
	void setPose(float _x, float _y, float _th);
	float findDistanceTo(Pose pos);
	float findAngleTo(Pose pos);
	Pose();
	~Pose();
private:
	float x;
	float y;
	float th;

};