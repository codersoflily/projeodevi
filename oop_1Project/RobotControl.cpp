#include"RobotControl.h"
#include"PioneerRobotAPI.h"
#include"Pose.h"

using namespace std;

void RobotControl::turnLeft() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}
void RobotControl::turnRight() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}
void RobotControl::forward(float speed) {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	robotAPI->moveRobot(speed);
}
void RobotControl::print() {
	print();
}
void RobotControl::backward(float speed ) {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
	robotAPI->moveRobot(speed);

}
Pose RobotControl::getPose() {
	position.getX();
	position.getY();
	position.getTh();
}
void RobotControl::setPose() {
	position.setX(100);
	position.setY();
	position.setTh(100);
}
void RobotControl::stopTurn() {
	//""
}
void RobotControl::stopMove() {
	robotAPI->stopRobot();
}