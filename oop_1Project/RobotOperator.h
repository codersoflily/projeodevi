#ifndef ROBOTOPERATOR_
#define ROBOTOPERATOR_
#include "Encryption.h"
#include<string>
using namespace std;

class RobotOperator {
private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
public:
	Encryption enc;
	int encryptCode(int number);
	int DecryptCode(int number);
	bool checkAccessCode(int number);
	void print();

};
#endif // !ROBOTOPERATOR_


