#pragma once
class Path
{
private:
	Node* tail;
	Node* head;
	int number;
public:
	void addPos(Pose pose);
	void print();
	operator[](int i);
	Pose getPos(int index);
	bool removePos(int index);
	bool insertPos(int index, Pose pose);
	operator<<();
	operator >> (pose);
};

