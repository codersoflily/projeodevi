#ifndef ROBOTCONTROL_
#define ROBOTCONTROL_
#include"PioneerRobotAPI.h"
#include"Pose.h"
//bu sat�r� sil (yucelsen ne yapaca��n� onura sor)
#include <iostream>
using namespace std;

class RobotControl {
private:
	Pose position;
	PioneerRobotAPI* robotAPI;
	int state;
public:
	void turnLeft();
	void turnRight();
	void forward(float speed);
	void print();
	void backward(float speed);
	Pose getPose();
	void setPose();
	void stopTurn();
	void stopMove();
};

#endif // !ROBOTCONTROL_
